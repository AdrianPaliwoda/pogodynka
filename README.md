Celem projektu było stworzenie aplikacji, która w łatwy i intuicyjny sposób dostarczy informacji o aktualnej pogodzie. Aplikacja powie nam, jaka jest temperatura- aktualny maksymalna i minimalna, prędkość wiatru i jego kąt, i powie nam, jaka jest ogólna pogoda. Dane dotyczące pogody są pobierane z Internetu i przechowywana w bazie danych.

Technologie:

- Java
- MySQL
- Mave
- Hibernate

GUI

Główne okno:

![Scheme](images/main.png)


Szczegóły pogody:

![Scheme](images/details.png)