package sample.util;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    public static SessionFactory sessionFact;
    static {
        try {
            // Create the SessionFactory from hibernate.cfg.xml
            sessionFact = new Configuration().configure().buildSessionFactory();
        }
        catch(Throwable e) {
            System.out.println("SessionFactory creation failed." + e);
            throw new ExceptionInInitializerError(e);
        }
    }
    public static final ThreadLocal<Session> session = new ThreadLocal<Session>();

    public static Session currentSession() throws HibernateException
    {
        Session sess = session.get();
        // Open a new Session, if this thread has none yet
        if(sess == null){
            sess = sessionFact.openSession();
            // Store it in the ThreadLocal variable
            session.set(sess);
        }
        return sess;
    }
    public static void sessionClose() throws Exception {
        Session s = session.get();
        if (s != null)
            s.close();
        session.set(null);
    }


    public static void save(Object object){
        Session session = HibernateUtil.currentSession();
        session.beginTransaction();
        session.saveOrUpdate(object);
        session.getTransaction().commit();
    }
}