package sample.Model;

import org.hibernate.Session;
import sample.util.HibernateUtil;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "weather")
public class Weather implements Serializable{
    @Id
    private Integer ID;
    @Column(name = "Name", length = 100)
    private String Name;
    @Column(name = "description", length = 100)
    private String description;
    @Column(name = "Temp", length = 100)
    private String Temp;
    @Column(name = "Temp_min", length = 100)
    private String Temp_min;
    @Column(name = "Temp_max", length = 100)
    private String Temp_max;
    @Column(name = "Wind_speed", length = 100)
    private String Wind_speed;
    @Column(name = "Wind_dig", length = 100)
    private String Wind_dig;


    public static sample.Model.Weather getWeaterFromBase(Integer number) throws Exception
    {
        Session session = HibernateUtil.currentSession();
        session = HibernateUtil.currentSession();
        sample.Model.Weather weather = (sample.Model.Weather) session.createQuery("FROM Weather where ID =" +
                " :id")
                .setParameter("id", number)
                .uniqueResult();
        HibernateUtil.sessionClose();
        return weather;
    }

    public static sample.Model.Weather getWeaterFromBase(String name) throws Exception
    {
        Session session = HibernateUtil.currentSession();
        session = HibernateUtil.currentSession();
        sample.Model.Weather weather = (sample.Model.Weather) session.createQuery("FROM Weather where Name =" +
                " :name")
                .setParameter("name", name)
                .uniqueResult();
        HibernateUtil.sessionClose();
        return weather;
    }

    public Weather()
    {
    }

    public Weather(String name, String description, String temp, String temp_min, String temp_max, String wind_speed, String wind_dig)
    {
        Name = name;
        this.description = description;
        Temp = temp;
        Temp_min = temp_min;
        Temp_max = temp_max;
        Wind_speed = wind_speed;
        Wind_dig = wind_dig;
    }

    public Weather(Integer ID, String name, String description, String temp, String temp_min, String temp_max, String wind_speed, String wind_dig)
    {
        this.ID = ID;
        Name = name;
        this.description = description;
        Temp = temp;
        Temp_min = temp_min;
        Temp_max = temp_max;
        Wind_speed = wind_speed;
        Wind_dig = wind_dig;
    }

    public Integer getID()
    {
        return ID;
    }

    public void setID(Integer ID)
    {
        this.ID = ID;
    }

    public String getName()
    {
        return Name;
    }

    public void setName(String name)
    {
        Name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getTemp()
    {
        return Temp;
    }

    public void setTemp(String temp)
    {
        Temp = temp;
    }

    public String getTemp_min()
    {
        return Temp_min;
    }

    public void setTemp_min(String temp_min)
    {
        Temp_min = temp_min;
    }

    public String getTemp_max()
    {
        return Temp_max;
    }

    public void setTemp_max(String temp_max)
    {
        Temp_max = temp_max;
    }

    public String getWind_speed()
    {
        return Wind_speed;
    }

    public void setWind_speed(String wind_speed)
    {
        Wind_speed = wind_speed;
    }

    public String getWind_dig()
    {
        return Wind_dig;
    }

    public void setWind_dig(String wind_dig)
    {
        Wind_dig = wind_dig;
    }

    @Override
    public String toString() {
        return "WeatherBasic{" +
                "ID=" + ID +
                ", Name='" + Name + '\'' +
                ", description='" + description + '\'' +
                ", Temp='" + Temp + '\'' +
                ", Temp_min='" + Temp_min + '\'' +
                ", Temp_max='" + Temp_max + '\'' +
                ", Wind_speed='" + Wind_speed + '\'' +
                ", Wind_dig='" + Wind_dig + '\'' +
                '}';
    }
}
