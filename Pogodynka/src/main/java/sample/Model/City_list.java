package sample.Model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "city_list")
public class City_list implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int ID;
    @Column(name = "Name", length = 100)
    private String Name;

    public City_list(String name)
    {
        Name = name;
    }

    public City_list()
    {
    }

    public int getID()
    {
        return ID;
    }

    public void setID(int ID)
    {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
