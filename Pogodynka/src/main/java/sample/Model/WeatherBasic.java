package sample.Model;

/**
 * Created by ddubi on 09.06.2017.
 */
public class WeatherBasic {

    private String name;
    private String id;
    private Weatherfeatures main;
    private Wind wind;
    WeatherDescription[] weather;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Weatherfeatures getMain() {
        return main;
    }

    public void setMain(Weatherfeatures main) {
        this.main = main;
    }

    public String getID()
    {
        return id;
    }

    public void setID(String ID)
    {
        this.id = ID;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public WeatherDescription[] getWeather() {
        return weather;
    }

    public void setWeather(WeatherDescription[] weather) {
        this.weather = weather;
    }

    @Override
    public String toString() {
        return "WeatherBasic{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", main=" + main +
                ", wind=" + wind +
                '}';
    }

    public sample.Model.Weather modelToBase()
    {
        sample.Model.Weather weatherBase = new sample.Model.Weather(Integer.parseInt(id),name,weather[0].getDescription(),main.getTemp(),main.getTemp_min(),main.getTemp_max(),wind.getSpeed(), wind.getDeg());
        return  weatherBase;
    }
}
