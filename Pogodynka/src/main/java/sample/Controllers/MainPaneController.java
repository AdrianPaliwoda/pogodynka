package sample.Controllers;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import sample.util.HibernateUtil;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public class MainPaneController {

    @FXML
    TextField cityName;
    @FXML
    Label message;
    @FXML
    Button showButton;

    private WeatherController weatherController;

    @FXML
    private void showWeather(ActionEvent event) throws Exception
    {
        String name = cityName.getText();
        sample.Model.Weather weather = null;

        if (netIsAvailable())
        {
            Session session = HibernateUtil.currentSession();
            Criteria criteria = session.createCriteria(sample.Model.City_list.class);
            criteria.add(Restrictions.like("Name", new String(name)));
            List<sample.Model.City_list> cities = criteria.list();
            if (cities.isEmpty())
            {
                message.setText("City not found");
                return;
            }

            String url = "http://api.openweathermap.org/data/2.5/weather?id=" +
                    cities.get(0).getID() + "&units=metric&appid=c437dbfb5a37d21751edd2eb1e005c18";
            weather = JsonController.parsToWeather(JsonController.readUrl(url)).modelToBase();

            session = HibernateUtil.currentSession();
            HibernateUtil.save(weather);
            HibernateUtil.sessionClose();
        } else
        {
            Session session = HibernateUtil.currentSession();
            Long count = (Long) session.createQuery("select count(1) from  Weather where Name = :name").setParameter
                    ("name", name)
                    .getSingleResult();
            if (count <= 0)
            {
                message.setText("City not found");
                return;
            }
            weather = sample.Model.Weather.getWeaterFromBase(name);
        }

        FXMLLoader loader = new FXMLLoader(this.getClass().getClassLoader().getResource("fxml/weatherPane.fxml"));
        ((Node) event.getSource()).getScene().setRoot((Parent) loader.load());
        weatherController = loader.getController();
        weatherController.setWeather(weather);
    }

    private static boolean netIsAvailable()
    {
        try
        {
            final URL url = new URL("http://www.google.com");
            final URLConnection conn = url.openConnection();
            conn.connect();
            return true;
        } catch (MalformedURLException e)
        {
            throw new RuntimeException(e);
        } catch (IOException e)
        {
            return false;
        }
    }

}