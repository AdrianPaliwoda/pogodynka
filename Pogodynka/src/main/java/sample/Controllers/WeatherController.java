package sample.Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.io.IOException;

/**
 * Created by adrian on 2017-06-11.
 */
public class WeatherController {

    @FXML
    Button backButton;
    @FXML
    Label cityName;
    @FXML
    Label temp;
    @FXML
    Label tempMin;
    @FXML
    Label tempMax;
    @FXML
    Label description;
    @FXML
    Label windSpeed;
    @FXML
    Label windDig;
    @FXML
    HBox WindDigH;
    @FXML
    HBox windSpeedH;

    private sample.Model.Weather weather;

    @FXML
    private void handlerBack(ActionEvent event) throws IOException
    {
        FXMLLoader loader = new FXMLLoader(this.getClass().getClassLoader().getResource("fxml/MainPanel.fxml"));
        ((Node)event.getSource()).getScene().setRoot((Parent)loader.load());
    }

    public void setWeather(sample.Model.Weather weatherIn)
    {
        weather = weatherIn;

        cityName.setText(weather.getName());
        temp.setText(weather.getTemp());
        tempMin.setText(weather.getTemp_min());
        tempMax.setText(weather.getTemp_max());
        description.setText(weather.getDescription());
        if(weather.getWind_speed()== null)
            windSpeedH.setVisible(false);
        else
            windSpeed.setText(weather.getWind_speed());
        if(weather.getWind_dig() == null )
            WindDigH.setVisible(false);
        else
            windDig.setText(weather.getWind_dig());
    }

}
